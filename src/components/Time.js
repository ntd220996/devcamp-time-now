import { Component } from "react";
import $ from "jquery";

class Time extends Component {
    constructor(props) {
        super(props);

        this.state = {
            time: new Date()
        }
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }
    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick = () => {
        this.setState({
            time: new Date()
        });
    }

    onChangeColorClick = () => {
        console.log('Thay đổi màu theo chẳn lẻ');

        let changeColor = new Date()

        { changeColor.getSeconds() % 2 === 0 ? ( $('#changeTextColor').css('color','red') )  : ($('#changeTextColor').css('color','blue')) }

        this.setState({
            time: new Date()
        })
    }

    render() {
        return (
            <div>
                <h1> Hello, World! </h1>
                <h2 id="changeTextColor"> It is {this.state.time.toLocaleTimeString()} </h2>
                <button  className="btn btn-warning" onClick={this.onChangeColorClick} > Change Color </button>
            </div>
        )
    }

}


export default Time