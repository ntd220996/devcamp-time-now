import Time from "./components/Time";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="text-center">
      <Time />
    </div>
  );
}

export default App;
